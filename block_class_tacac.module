<?php

/**
 * @file
 * Drupal hooks and global API functions.
 */

/**
 * Implements hook_form_alter().
 */
function block_class_tacac_form_alter(&$form, &$form_state, $form_id) {
  if (user_access('administer block classes') && ($form_id == 'block_admin_configure' || $form_id == 'block_add_block_form')) {
    $form['settings']['css_class']['#autocomplete_path'] = 'css_classes/autocomplete';
    $form['settings']['css_class']['#default_value'] = str_replace(" ", ", ", $form['settings']['css_class']['#default_value']);
    $form['settings']['css_class']['#description'] = t('Customize the styling of this block by adding CSS classes. Separate multiple classes by comma.');

    $form['#element_validate'][] = 'taxonomy_autocomplete_validate';
    $form['#validate'][] = 'block_class_tacac_form_validate';
    $form['#submit'][] = 'block_class_tacac_form_submit';
  }
}

/**
 * Implements hook_menu().
 */
function block_class_tacac_menu() {
  $items['css_classes/autocomplete'] = array(
    'title' => '',
    'description' => '',
    'page callback' => 'block_class_tacac_autocomplete',
    'access arguments' => array('administer block classes'),
    'page arguments' => array(2),
    'type' => MENU_CALLBACK,
    'behaviors' => array(
      'multiple values' => TRUE,
    ),
  );

  return $items;
}

/**
 * Implements autocomplete logic.
 */
function block_class_tacac_autocomplete($string) {
  $matches = array();

  if ($string) {
    $items = array_map('trim', explode(',', $string));
    $tag_last = array_pop($items);
    $prefix = implode(', ', $items);

    $vocabulary = taxonomy_vocabulary_machine_name_load('css_classes');

    $result = db_select('taxonomy_term_data', 't')
      ->fields('t', array('tid', 'name'))
      ->condition('vid', $vocabulary->vid, '=')
      ->condition('name', db_like($tag_last) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();

    foreach ($result as $term) {
      if (!in_array($term->name, $items)) {
        $value = !empty($prefix) ? $prefix . ', ' . $term->name : $term->name;
        $matches[$value] = check_plain($term->name);
      }
    }

    drupal_json_output($matches);
  }
}

/**
 * Implements custom form #validate.
 */
function block_class_tacac_form_validate($form, &$form_state) {
  form_set_value($form['settings']['css_class'], str_replace(",", "", $form['settings']['css_class']['#value']), $form_state);
}

/**
 * Implements custom form #submit.
 */
function block_class_tacac_form_submit($form, &$form_state) {
  $vocabulary = taxonomy_vocabulary_machine_name_load('css_classes');
  $terms = explode(",", $form['settings']['css_class']['#value']);

  $terms = array_filter($terms);

  foreach ($terms as $term) {
    $newterm = new stdClass();
    $newterm->name = trim($term);
    $newterm->vid = $vocabulary->vid;

    $find = taxonomy_get_term_by_name($newterm->name, $vocabulary->machine_name);

    if (count($find) == 0) {
      taxonomy_term_save($newterm);
    }
  }
}

/**
 * Implements hook_help().
 */
function block_class_tacac_help($path, $arg) {
  switch ($path) {
    case 'admin/help#block_class_tacac':

      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($filepath);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }

      return $output;
  }
}
