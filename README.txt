CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration



 INTRODUCTION
-------------

This module creates a taxonomy vocabulary to store CSS Classes and use it for 
autocompleting on the Block Class module field.


REQUIREMENTS
------------

Drupal 7.x

Block Class - https://www.drupal.org/project/block_class


INSTALLATION
------------

1. Copy the entire Block Class Taxonomy Autocomplete directory the Drupal 
sites/all/modules directory.

2. Login as an administrator. Enable the module in the 
"Administer" -> "Modules".


CONFIGURATION
-------------

No need to configure.
